#!/usr/bin/bash
# by nu11secur1ty @ 2016

echo "input domain"
   read D
echo "input your DNS"
   read DN

# with nslookup
echo "check with nslookup..."
nslookup -norecurse $D $DN

# with dig
echo "check with dig..."
   sleep 5
dig $DN $D A +norecurse

exit 0;
